const dotenv = require('dotenv').config();

module.exports = {
  development: {
    type: 'development',
    port: process.env.PORT,
    mongodb: process.env.DATABASE_URL
    },
  production: {
    type: 'production',
    port: process.env.PORT,
    mongodb: process.env.DATABASE_URL
  }
}